from django.test import TestCase
from ..models import ListItem


class ListItemTest(TestCase):
	""" Test module for ListItem model """

	def setUp(self):
		self.i1 = ListItem.objects.create(title='Visit army camp', description='I would like to visit the army camp')
		self.i2 = ListItem.objects.create(title='Visit Mt. Everest', description='I would like to visit Mt. Everest')

	def test_ListItem_breed(self):
		li1 = ListItem.objects.get(id=self.i1.id)
		li2 = ListItem.objects.get(id=self.i2.id)
		
		self.assertEqual(li1.get_item(), self.i1.id)
		self.assertEqual(li2.get_item(), self.i2.id)