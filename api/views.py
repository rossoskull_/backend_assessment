from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import ListItem
from .serializers import ListItemSerializer


@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_list_item(request, pk):
	try:
		listitem = ListItem.objects.get(id=pk)
	except ListItem.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	# get details of a single list_item
	if request.method == 'GET':
		return Response(ListItemSerializer(listitem).data)
	# delete a single list_item
	elif request.method == 'DELETE':
		listitem.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)
	# update details of a single list_item
	elif request.method == 'PUT':
		serializer = ListItemSerializer(listitem, data=request.data)
		if serializer.is_valid():
			serializer.save()
		return Response(serializer.data, status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def get_post_list_items(request):
	# get all list items
	if request.method == 'GET':
		return Response(ListItemSerializer(ListItem.objects.all(), many=True).data)
	# insert a new record for a list_item
	elif request.method == 'POST':
		data = {
			'title': request.data.get('title'),
			'description': request.data.get('description')
		}

		serializer = ListItemSerializer(data = data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)