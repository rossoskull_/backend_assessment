from django.db import models

# Create your models here.
class ListItem(models.Model):
  title = models.CharField(max_length=255)
  description = models.CharField(max_length=255)
  created_at = models.DateTimeField(auto_now_add=True)

  def get_item(self):
    return self.id

  def __repr__(self):
    return self.title + ' is created.'