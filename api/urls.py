from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        r'^list_items/(?P<pk>[0-9]+)$',
        views.get_delete_update_list_item,
        name='get_delete_update_list_item'
    ),
    url(
        r'^list_items/$',
        views.get_post_list_items,
        name='get_post_list_items'
    )
]