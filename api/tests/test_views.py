import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import ListItem
from ..serializers import ListItemSerializer

# initialize the APIClient app
client = Client()


''' Test to get all list items '''
class GetAllListItemsTest(TestCase):
	def setUp(self):
		ListItem.objects.create(title='abc', description='lorem ipsum')
		ListItem.objects.create(title='cba', description='lorem ipsum dolor sit')

	def test_fetch_all_listitems(self):
		response = client.get(reverse('get_post_list_items'))

		listitems = ListItem.objects.all()
		serializer = ListItemSerializer(listitems, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


''' Test to fetch a single list item'''
class GetSingleListItemTest(TestCase):
	def setUp(self):
		self.li1 = ListItem.objects.create(title='abc', description='lorem ipsum')
		self.li2 = ListItem.objects.create(title='cba', description='lorem ipsum dolor sit')

	def test_fetch_single_exists(self):
		response = client.get(reverse('get_delete_update_list_item', kwargs={'pk': self.li1.id}))

		listitem = ListItem.objects.get(id=self.li1.id)
		serializer = ListItemSerializer(listitem)

		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_fetch_single_doesnt_exist(self):
		response = client.get(reverse('get_delete_update_list_item', kwargs={'pk':'4'}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


''' Test for the creation of a single list item '''
class CreateListItemTest(TestCase):
	def setUp(self):
		self.valid = {'title': 'valid list item', 'description': 'this is a valid list item'}

		self.invalid = {'title': 'This is an invalid list item'}

	def test_create_list_item_success(self):
		response = client.post(
			reverse('get_post_list_items'),
			data=json.dumps(self.valid),
			content_type='application/json'
		)

		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_list_item_fail(self):
		response = client.post(reverse('get_post_list_items'), data=json.dumps(self.invalid), content_type='application/json')

		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


''' Test for deletion of a list item '''
class DeleteListItemTest(TestCase):
	def setUp(self):
		self.li = ListItem.objects.create(title='Deletable', description='Lore ipsum')

	def test_delete_item_exists(self):
		response = client.delete(reverse('get_delete_update_list_item', kwargs={'pk': self.li.id}))

		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_delete_item_doesnt_exist(self):
		response = client.delete(reverse('get_delete_update_list_item', kwargs={'pk': '100'}))

		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


''' Test for updating a list item '''
class UpdateListItemTest(TestCase):
	def setUp(self):
		self.li = ListItem.objects.create(title='existing list item', description='this is an existing list item')

		self.valid = {'title': 'valid list item', 'description': 'this is a valid list item'}

		self.invalid = {'title': 'This is an invalid list item'}

	def test_update_list_item_valid(self):
		response = client.put(reverse('get_delete_update_list_item', kwargs={'pk': self.li.id}), data=json.dumps(self.valid), content_type='application/json')

		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_update_list_item_invalid(self):
		response = client.put(reverse('get_delete_update_list_item', kwargs={'pk': 100}), data=json.dumps(self.valid), content_type='application/json')

		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)